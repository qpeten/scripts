#!/bin/bash
for snapshot in `sudo zfs list -r -o space VAULT |grep '@' |grep backup|cut -f 1 -d ' '`
do
  echo $snapshot
  sudo zfs destroy $snapshot
done
