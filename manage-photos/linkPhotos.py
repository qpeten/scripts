#!/usr/bin/python3
# -*- coding: utf-8 -*-


import os
import sys


desiredState = {
  '240525 240602 - Les Collons - Chalet-working avec papa' : 'i',
  '240715 240721 - Les Collons - Les Petens au chalet' : 'i',
  '240427 240504 - Collons - Le Linux en Suisse' : 'linux',
  '230818 230823 - Autriche - Vacances en velo' : 'i',
  '230419 - LLN - Circokot - Midi-minuit' : 'i',
  '221020 221030 - Maroc - Croisons nos chemins 10 ans' : 'i',
  '220716 220728 - Irlande - Vacances en famille' : 'i',
  '211031 - Bruxelles - Fiancailles Emile et Ciara' : 'b',
  '211024 - Havelange - 160 ans grands-parents' : 'l',
  '210101 210109 - Val Cenis - Ski de Rando' : 'i',
  '200000 - Anderlecht - Travaux' : "i",
  '191117 - Ferme de Holleken - Soiree Aymeric' : 'i',
  '190701 190713 - Srilanka - Vacances' : "i",
  '190119 190126 - Val Cenis - Ski' : "l",
  '180909 180916 - Suede - Canoe' : "i",
  '180728 180801 - Vercors - Croisons nos chemins' : "i",
  '180210 180218 - Canada - Chiens de traineau' : "i",
  '171028 171104 - Norvege - Vacances Parents' : "i",
  '170701 170712 - Montenegro - Vacances en famille' : "i",
  '170301 170303 - Venise - Vacances' : "i",
  '170211 - Feluy - Soiree Agnes' : 'i',
  '170128 170204 - Val Cenis - Skinux' : 'linux',
  '160906 160911 - Budapest - Vacances avec Kot' : 'linux',
  '161224 - Lennik - Photo famille' : 'l',
  '160702 160712 - Italie - Vacances en famille' : 'i',
  '160324 - Rallye Chambre' : 'linux',
  '160319 160320 - Koksijde-Bad - Week-end Kot LLnux' : 'linux',
  '160123 160130 - 2 Alpes - Ski Linux' : 'linux',
  '151299 - LLN - Blocus avec Boris' : 'linux',
  '151215 - Seneffe - Maison' : 'i',
  '151122 - Seneffe - St Nicolas' : 'm',
  '151114 - Frasnes - Soiree Sofflufy' : 'linux',
  '151011 - Kot - Shooting Geek' : 'linux',
  '150724 150729 - France - Vacances CAF Casa' : 'i',
  '150701 150714 - Tanzanie' : 'i',
  '140914 - Lennik - Fete Walkiers' : 'l',
  '140906 140913 - Madere - Randonnee' : 'i',
  '140518 - Seneffe - Communion Aymeric' : 'l',
  '130922 - Lennik - Week-end Famillial' : 'l',
  '130802 130819 - France-Espagne - Vacances ac amis Casa' : 'i',
  '121030 - Bruxelles - Visite ac Josse' : 'i',
  '120915 120916 - Ardennes - B70 - WE famillial' : 'l',
  '120723 120808 - Chine - Vacances ac Belpaire' : 'i',
  '120414 - Maroc-Mhamid - Randonnee desert' : 'i',
  '120225 - Maroc - Fauconniers - Ancienne Chellah' : 'i',
  '111231 120101 - Toulouse - Reveillon' : 'i',
  '111222 - Lennik - Noel Peten' : 'l',
  '111101 111105 - Maroc - Randonnee en vtt' : 'i',
  '111029 - Maroc - Fez avec Belpaire et Guillaume' : 'i',
  '110821 - Espagne - Rafting' : 'i',
  '110812 110830 - Espagne-Portugal - Vacances Famille' : 'i',
  '110803 110807 - Maroc - Randonnee ac grands-parents' : 'i',
  '110709 110710 - Lennik - WE famillial' : 'l',
  '110529 - Lennik - Profession de foi Cédric' : 'l',
  '110515 - Lennik - Paques Peten' : 'l',
  '110424 - Maroc-El Jadida - Citerne et plage' : 'i',
  '110421 - Maroc - Surf ac Jean-Pierre' : 'i',
  '110418 110419 - Maroc - Surf ac Fabienne et Jean-Pierre' : 'i',
  '110417 - Maroc - Caf - canoe et randonee' : 'i',
  '110405 110410 - Espagne - Voyage en Andalousie' : 'i',
  '101225 - Lennik - Ballade dans la neige' : 'l',
  '100821 100826 - Maroc - Nord et Rif' : 'i',
  '100802 - Maroc - Vacances Ghilsaine et Baudouin' : 'i',
  '100728 - Plage Casa - Surf' : 'i',
  '100627 - Enghien - Communion Jumelles' : 'l',
  '100523 - Bruxelles - Mariage Fabienne et Jean-Pierre' : 'l',
  '100503 - Seneffe - Proffession de foi Agnes - Communion Aymeric - Anniversaire Quentin' : 'l',
  '100502 - Seneffe - Gilles et Nina - Prof de foi Agnes - Communion Aymeric - Anniversaire Quentin' : 'i',
  '100419 - Lennik - Paques Peten' : 'l',
  '100411 100415 - Maroc - Balade - Appart papa' : 'i',
  '100116 - Lennik - Noel' : 'l',
  '091129 - Seneffe - St Nicolas Maes' : 'm',
  '090914 - Lennik - Fete Walkiers' : 'l',
  '090913 - Lennik - Bapteme Charlotte et Heloise' : 'l',
  '090823 - Seneffe - Photo Aymeric Arbre' : 'i',
  '090812 - Seneffe - Aymeric Quentin - Tond la pelouse' : 'i',
  '090812 - Seneffe - Aymeric dort dans beaucoup de couvertures' : 'i',
  '090808 - Lac de leau dheure - Matthieu et Nicky planche a voile' : 'i',
  '090718 090730 - France-Italie - Vacances - Canyonning' : 'i',
  '090620 - Bruxelles - Musee Archiborescence Luc Schuiten' : 'i',
  '090618 - Seneffe - Bac a Sable - Aymeric ensable' : 'i',
  '090601 - Seneffe - Celine et Nicky - Ballade en velo' : 'i',
  '090530 - Nivelles - Nuit pour Dieu' : 'i',
  '090529 - Lennik - Paques ac toute la famille' : 'l',
  '090517 - Lennik - Anniversaire Mai 09' : 'l',
  '090513 - Seneffe - Anniversaire Aymeric' : 'i',
  '090510 - Bruxelles - Naissance Heloise' : 'l',
  '090502 - Seneffe - Anniversaire Quentin - Monopoly' : 'i',
  '090501 - Bruxelles - Soiree chant Benedicte' : 'i',
  '090415 - Seneffe - Enfants Gilles Jardin' : 'm',
  '090413 - Lennik - Paques' : 'l',
  '090411 - Seneffe - Divers Maison trempoling' : 'i',
  '090410 - Seneffe - Trempoline Matthieu Marcel' : 'i',
  '090320 - Paris - Voyage Qt Mt Alexis' : 'm',
  '090318 - Seneffe - Enfants avec Mariange' : 'i',
  '090311 - Seneffe - Nuit arbres dessous - Lune rouge' : 'i',
  '090308 - Seneffe - Balade en velo' : 'i',
  '090307 - Seneffe - Matthieu Monocycle' : 'i',
  '090110 - Ronquieres - Patinoire sur etang Linard' : 'i',
  '090103 - Seneffe - Patinoire sur le canal' : 'i',
  '081023 - Seneffe - Flash Matthieu trempoline' : 'i',
  '080925 - Matthieu monocycle' : 'i',
  '080921 - Chevetogne - Fete Afton' : 'i',
  '080817 - Seneffe - Balade en velo' : 'i',
  '080815 - Lennik - Jeux camion' : 'l',
  '080814 - Seneffe - Balade en velo' : 'i',
  '080720 080803 - Maroc - Randonnee dans l_Atlas' : 'i',
  '080515 - Seneffe - Aniversaire matthieu et Aymeric' : 'i',
  '080515 - Seneffe - Aniversaire matthieu et Aymeric' : 'i',
  '080514 - Seneffe - Soiree' : 'i',
  '080513 - Seneffe - Fleurs mur maison' : 'i',
  '080512 - Lennik - Piscine - Fleurs' : 'l',
  '080511 - Enghien - Profession de foi Manon' : 'l',
  '080426 - Seneffe - Jardin' : 'i',
  '080420 - Seneffe - Profession de foi matthieu' : 'i',
  '080420 - Seneffe - Photo Matthieu aube' : 'i'
}

path_Quentin = "/mnt/docker-data/photoview/photos/all/Quentin/"
path_folders = {
  'b' : '/mnt/docker-data/photoview/photos/all/Beguin',
  'i' : "/mnt/docker-data/photoview/photos/all/Ittre",
  'l' : "/mnt/docker-data/photoview/photos/all/Lennik",
  'm' : '/mnt/docker-data/photoview/photos/all/Maes',
  'p' : '/mnt/docker-data/photoview/photos/all/Parents',
  'linux' : '/mnt/docker-data/photoview/photos/all/Linux'
}

for currentDir in desiredState:
  source = os.path.join(path_Quentin, currentDir)

  if not os.path.isdir(source):
    print("Source path does not exist: ", source)
    sys.exit()

  destination = None
  if not desiredState[currentDir] in path_folders:
    print("Unknown destination folder : ", currentDir, desiredState[currentDir])

  destination = os.path.join(path_folders[desiredState[currentDir]], os.path.basename(os.path.normpath(source)))

  if not os.path.isdir(destination) and not os.path.isfile(destination):
    print("Hard-linking : ", currentDir)

    os.mkdir(destination)
    source_files = os.listdir(source)

    for f in source_files:
      src_file = os.path.join(source, os.path.basename(f))
      dst_file = os.path.join(destination, os.path.basename(f))
      os.link(src_file, dst_file)
