#!/bin/bash
#Checks if shairport-sync service port is open.
#If it isn't, assumes the rpi has failed and foreces a restart
if [[ $HOSTNAME == 'loki' ]]; then
  expectArgs="us-16-poe-gen2 10.0.50.33"
  nc -z rpi-salonAudio 22
  if [[ ! $? -eq 0 ]]; then
    ./restartRpi-new.exp $expectArgs G4
  fi
elif [[ $HOSTNAME == 'fileServer' ]]; then
  expectArgs="us-8-150-Escalier 192.168.1.252"
  nc -z 192.168.1.250 22 #Couloir bas
  if [[ ! $? -eq 0 ]]; then
    ./restartRpi.exp $expectArgs 0/2
  fi
  nc -z 192.168.1.253 22 #Chaudière
  if [[ ! $? -eq 0 ]]; then
    ./restartRpi.exp $expectArgs 0/3
  fi
else
  echo "Error : unkown local hostname"
fi
